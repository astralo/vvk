;
(function ($, window, document, undefined) {
  //scrollTo
  $(".ar-main-nav a").on("click", function () {
    $('body,html').animate({scrollTop: $($(this).attr("href")).offset().top - 134}, 800);
    // return false;
  });
  //end scrollTo

  //mask
  $('.form-phone').mask('0(000)-000-00-00', {placeholder: "+7 ( ___ ) - ___ - __ - __"});
  //end mask

  // Feeds
  setTimeout(function () {
    var heights = [];
    $(".ar-feeds-owner>div .-text").each(function (indx) {
      $(this).css({"bottom": -$(this).innerHeight() + 47});
    });
  }, 2000);

  $('.ar-feeds-owner>div').hover(
    function () {
      var feed = $(this).find(".-text");
      TweenLite.to(feed, 0.3, {bottom: 0, ease: Power2.easeIn});
    }, function () {
      var feed = $(this).find(".-text");
      var indent = -$(this).find(".-text").innerHeight() + 47
      TweenLite.to(feed, 0.3, {bottom: indent, ease: Power2.easeIn});
    });
  // end Feeds

  // points map
  $(".ar-point-list>li>a").hover(
    function () {
      $("#" + $(this).attr("href")).find(".ar-points-data").show();
    }, function () {
      $("#" + $(this).attr("href")).find(".ar-points-data").hide();
    });

  $(".ar-feleals .-points").hover(
    function () {
      $(this).find(".ar-points-data").show();
      $(".ar-point-list").find(".list-" + $(this).attr("id")).addClass("active")
    }, function () {
      $(this).find(".ar-points-data").hide();
      $(".ar-point-list>li>a").removeClass("active")
    });
  // end points map

  // map initialization
  var gMapInitialize = function (map, x, y) {
    var myLatlng = new google.maps.LatLng(x, y),
      myOptions = {
        zoom: 17,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: false,
        panControl: false,
        zoomControl: true,
        zoomControlOptions: {
          position: google.maps.ControlPosition.LEFT_CENTER
        },
        scrollwheel: false
      },
      map = new google.maps.Map(map, myOptions),
      marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title: "Hotel Gudauri",
        animation: google.maps.Animation.DROP
      });
    return map;
  };
  var $gMap = $("#map");
  if ($gMap.length) {
    var mapMap = gMapInitialize($gMap[0], 55.923382, 37.5265771);
  }
  // end map

  // paralax
  var $bgobj = $('.-paralax');
  $(window).scroll(function () {
    if ($(window).scrollTop() > $('.-paralax').offset().top) {
      var yPos = (($(window).scrollTop() - $('.-paralax').offset().top));
      var coords = '50% ' + yPos + 'px';
      $bgobj.css({ 'backgroundPosition': coords });
    } else {
      $bgobj.css({ 'backgroundPosition': '50%  0' });
    }
    ;
  });
  // end paralax

  var $fixed = $('.header');
  $(window).scroll(moveFixedLeft);
  $(window).resize(moveFixedLeft);
  function moveFixedLeft() {
    var b = document.body;
    var d = document.documentElement;
    var s = window.pageXOffset || d.scrollLeft || b.scrollLeft;
    if (s !== 0) $fixed.css('left', -s);
    return false;
  }
})(jQuery, window, document);